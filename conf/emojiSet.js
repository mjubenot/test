
const emojisPlayerChoice= {
    "1⃣":{
        name:"1",
        indexValue:0
    },
    "2⃣":{
        name:"2",
        indexValue:1
    },
    "3⃣":{
        name:"3",
        indexValue:2
    },
    "4⃣":{
        name:"4",
        indexValue:3
    },
    "5⃣":{
        name:"5",
        indexValue:4
    },
    "❌":{
        name:"Cancel",
        indexValue:-1
    },
}

const emojisYesNo= {
    "✅":{
        name:"Yes"
    },
    "❌":{
        name:"No"
    },
}